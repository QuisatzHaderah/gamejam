﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {
	
	public float movementSpeed = 5;
	public float jumpSpeed = 200;
	public bool isJumping = false;

	void Update () {

		if(Input.GetKey(KeyCode.RightArrow))
		{
			transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
		}
		else if (Input.GetKey("left"))
		{
			transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
		}

		if(Input.GetKey("up") )
		{
			if(!isJumping)
			{
				rigidbody2D.AddForce(Vector2.up * jumpSpeed);
				isJumping = true;
			}
		}
		/*else if (Input.GetKey("down"))
		{
		}*/

	
	}

	void OnCollisionStay2D(Collision2D other)
	{

		if(other.gameObject.name.Equals("ortam"))
			isJumping = false;
	}


}
