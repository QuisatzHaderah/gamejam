﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	Vector3 temp;
	float xDamping = 2.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float currentX = transform.position.x;
		currentX = Mathf.Lerp (currentX, 1f, xDamping * Time.deltaTime);
		temp = new Vector3(currentX, transform.position.y, transform.position.z);
		this.transform.position = temp;
	
	}
}
